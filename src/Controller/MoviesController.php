<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MoviesController extends AbstractController
{
    private $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    #[Route('/movies', name: 'app_movies')]    
    /**
     * index
     *
     * @return JsonResponse
     */
    public function index($name=null): JsonResponse
    {
        return $this->json([
            'message' => $name,
            'path' => 'src/Controller/MoviesController.php',
        ]);
    }

    #[Route('/movies/tampilan', name: 'app_tampilan')]
    public function tampilan(): Response
    {
        //findAll() - Select * from movies;
        //find($param) - select * from movies WHERE id = $param

        $repository = $this->em->getRepository(Movie::class);
        $movies = $repository->findBy([], ['id'=>'DESC']);
        dd($movies);
        return $this->render('index.html.twig');
    }
} 
